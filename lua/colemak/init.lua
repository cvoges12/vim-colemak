local M = {}

local function bind(f,...)
    local args={...}
    return function(...)
        return f(unpack(args),...)
    end
end

function M.modes_map(modes)
    local functions = {}
    for _,mode in pairs(modes) do
        table.insert(functions, bind(vim.api.nvim_set_keymap,mode))
    end
    return functions
end

function M.keys_modes_map(modes, new_keys, old_keys)
    for i = 1, #new_keys do
        for _,f in pairs(M.modes_map(modes)) do
            f(new_keys[i], old_keys[i], {noremap = true})
        end
    end
end

function M.map()
    -- Motion
    -- up(e) down(n) left(h) right(i)
    M.keys_modes_map(
        {'n','v','o'},
        {'h','n','e','i'},
        {'h','j','k','l'}
    )

    -- word(y) WORD(Y) back(l) BACK(L) end(u) END(U)
    M.keys_modes_map(
        {'n','v','o'},
        {'l','L','u','U','gu','gU','y','Y'},
        {'b','B','e','E','ge','gE','w','W'}
    )

    -- (a)ppend (r)eplace in(s)ert change(w)
    M.keys_modes_map(
        {'n'},
        {'s','S'},  -- see Visual Line Mode Patch
        {'i','I'}
    )
    M.keys_modes_map(
        {'n','v','o'},
        {'w','W'},
        {'c','C'}
    )

    -- (c)opy (p)aste cut(x,d)
    M.keys_modes_map(
        {'n','v','o'},
        {'c','C'},
        {'y','Y'}
    )

    -- (u)ndo (r)edo
    local c_undo_redo = {'z','gz','Z'}
    M.keys_modes_map(
        {'n'},
        c_undo_redo,
        {'u','U','<C-R>'}
    )
    local q_x_undo = ':<C-U>undo<CR>'
    M.keys_modes_map(
        {'x'},
        c_undo_redo,
        {
            q_x_undo,
            q_x_undo,
            ':<C-U>redo<CR>'
        }
    )

    -- Visual Line Mode Patch
    vim.cmd([[
        xnoremap <silent> <expr> s (mode() =~# "[V]" ? "\<C-V>0o$I" : "I")
        xnoremap <silent> <expr> S (mode() =~# "[V]" ? "\<C-V>0o$I" : "I")
    ]])

    -- (g)oto
    -- ge = visual line up
    -- gn = visual line down
    M.keys_modes_map(
        {'n','v','o'},
        {'ge','gn'},
        {'gk','gj'}
    )

    -- Search
    -- (f)ind (F)ind (t)il (T)il next(k) prev(K)
    M.keys_modes_map(
        {'n','v','o'},
        {'k','K'},
        {'n','N'}
    )

    -- Text Objects
    -- inner(s) (a)
    vim.api.nvim_set_keymap('o', 's', 'i', {noremap = true})

    -- Folds
    M.keys_modes_map(
        {'n','x'},
        {'j','jn','je','jo','jc','ja'},
        {'z','zn','ze','zo','zc','za'}
    )

    -- Help(B)
    vim.api.nvim_set_keymap('n', 'B', 'K', {noremap = true})

    -- Splits
    local split = {
        colemak = {'n','e','i','N','E','I','h','H','q','','v','^','ge','q','o','w','W','t','b','p','P','r','R','x','T','=','-','+','_','<','>','|',']','g]','f','F','gf','gF','gt','gT','z','}','g}','s','S','v','^','ge','c'},
        qwerty  = {'j','k','l','J','K','L','h','H','q','','v','^','ge','q','o','w','W','t','b','p','P','r','R','x','T','=','-','+','_','<','>','|',']','g]','f','F','gf','gF','gt','gT','z','}','g}','s','S','v','^','ge','c'}
    }
    for i = 1, #split.colemak do
        for _,f in pairs(M.modes_map({'n','v'})) do
            f('<Leader>w'..split.colemak[i], '<C-W>'..split.qwerty[i], {noremap = true})
        end
    end

    -- Screen / Scroll / Page
    -- H = top screen
    -- M = mid screen
    -- I = bottom screen
    -- E = half page up
    -- N = half page down
    M.keys_modes_map(
        {'n','v'},
        {'I','E',    'N'},
        {'L','<C-U>','<C-D>'}
    )
end

return M
