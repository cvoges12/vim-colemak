if exists("g:loaded_colemak")
    finish
endif

let g:loaded_colemak = 1

command! Colemak lua require('colemak').map()
